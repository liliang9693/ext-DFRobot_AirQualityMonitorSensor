# Air Quality Monitor 空气质量监测仪 - PM2.5, 甲醛, 温湿度



<img src="https://github.com/liliang9693/ext-DFRobot_AirQualityMonitorSensor/blob/master/arduinoC/_images/featured.png" width="450" height="300" align=center>

---------------------------------------------------------


## 链接
- **本用户库加载地址:** ```https://github.com/liliang9693/ext-DFRobot_AirQualityMonitorSensor```
- **Mind+下载地址：**```http://mindplus.cc```

## 简介
- **产品链接：** ```https://www.dfrobot.com.cn/goods-1422.html```  
- **介绍：** 本扩展库为DFRobot空气质量监测仪 (PM2.5, 甲醛, 温湿度)传感器（SEN0233）设计，从Mind+导入本库。

## Blocks

![image](https://github.com/liliang9693/ext-DFRobot_AirQualityMonitorSensor/blob/master/arduinoC/_images/block.png)

## 示例
![image](https://github.com/liliang9693/ext-DFRobot_AirQualityMonitorSensor/blob/master/arduinoC/_images/example.png)



## License

MIT

## 硬件支持

MCU                | JavaScript    | Arduino   | MicroPython    | Remarks
------------------ | :----------: | :----------: | :---------: | -----
micro:bit        |             |       √       |             | 
mpython        |             |        √      |             | 
arduino uno    |             |        √      |             | 

## 更新日志
- V0.1.0 20200128 第一版发布
- V0.1.1 20200518 根据163升级


